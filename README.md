# README #

My basic Gulpfile to develop a HTML5 & CSS3 websites.

## It uses these packages ##

I use the following packages in my basic Gulp workflow:

* [gulp-sass](https://www.npmjs.com/package/gulp-sass)
* [gulp-autoprefixer](https://www.npmjs.com/package/gulp-autoprefixer) 
* [gulp-clean-css](https://www.npmjs.com/package/gulp-clean-css) 
* [browser-sync](https://www.npmjs.com/package/browser-sync)
