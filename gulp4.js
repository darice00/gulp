  var gulp = require('gulp');
  var sass = require('gulp-sass');
  var autoprefixer = require('gulp-autoprefixer');
  var cleanCSS = require('gulp-clean-css');


  var input = 'sass/*.scss';
  var output = 'css/';


  const compileSass = function(){    
    return gulp.src(input)       
        .pipe(sass())       
        .pipe(autoprefixer('last 2 version'))
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(output)); 
}    

gulp.task('sass', compileSass);    


gulp.task('default', function() {
    gulp.watch(input, gulp.series('sass'));
})


    gulp.task('minify', function(){    
        return gulp.src(input)       
            .pipe(sass())           
            .pipe(autoprefixer('last 2 version'))
            .pipe(cleanCSS({debug: true}, function(details) {
                console.log(details.name + ': ' + details.stats.originalSize);
                console.log(details.name + ': ' + details.stats.minifiedSize);
            }))
            .pipe(gulp.dest(output)); 
    });

