var gulp = require('gulp');
    sass = require('gulp-sass');
    prefix = require('gulp-autoprefixer');
    cleanCSS = require('gulp-clean-css');
    browserSync = require('browser-sync').create();


    var input = 'sass/**/*.scss'; //sass folder
    var output = 'css/';          //css folder

      gulp.task('serve', ['sass'], function() {

        browserSync.init({
            server: "./" //base directory localhost:3000 is the working ulr. project folder is not root
  });

            gulp.watch(input, ['sass']);
            gulp.watch("./*.html").on('change', browserSync.reload);
                        //check for all html files in the project folder
    });


        gulp.task('sass', function () {
           gulp   //add gulp without return to keep session going
            // Find all `.scss` files from the `sass/` folder
            .src(input)
            // Run Sass on those files || pipe adds everything together
            .pipe(sass().on('error', sass.logError)) //error log to keep session going when scss contains error
            .pipe(prefix( 'last 2 version' ) )
            // Write the resulting CSS in the output folder
            .pipe(gulp.dest(output))
            //refresh browser
            .pipe(browserSync.stream());
        });

        //run 'gulp' in terminal to get everything going
        gulp.task('default', ['serve']);

       //run gulp minify-css in terminal to minify css for production
        gulp.task('minify-css', function() {
            return gulp.src('css/*.css')
                .pipe(cleanCSS({debug: true}, function(details) {
                    console.log(details.name + ': ' + details.stats.originalSize);
                    console.log(details.name + ': ' + details.stats.minifiedSize);
                }))
                .pipe(gulp.dest(output));
        });
